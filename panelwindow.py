from PyQt5.QtWidgets import QWidget, QLabel
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPixmap
import os
from widgets import ImageLabelWithMouseListener


class SwitchablePanelWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.data_models = []
        self.initUI()

    def initUI(self):
        self.show()

    def set_current_panel(self, num):
        if num not in range(len(self.data_models)+1):
            print('Panel num must be between 1-6')

        try:
            self.current_model = self.data_models[num]
        except IndexError:
            pass

    def add_model(self, model):
        self.data_models.append(model)

    def hide_images(self):
        for child_widget in self.findChildren(QWidget):
            child_widget.hide()

    def add_images(self):
        self.component_array = []

        try:
            background_image, xy_pos = self.current_model.get_background()
            backgrnd_label = QLabel(self)
            backgrnd_pixmap = QPixmap(background_image)
            backgrnd_label.setPixmap(backgrnd_pixmap)
            backgrnd_label.setGeometry(0, 0, backgrnd_pixmap.width(), backgrnd_pixmap.height())
            backgrnd_label.show()

            for control in self.current_model.get_control_data():
                if control.image_path:
                    pixmap = QPixmap(control.image_path)
                    label = ImageLabelWithMouseListener(self, pixmap, control.name)

                    x = int(control.xy_position[0])
                    y = int(control.xy_position[1])

                    if hasattr(control, 'num_frames'):
                        height = pixmap.height() / int(control.num_frames.text)
                    else:
                        height = pixmap.height()

                    label.setGeometry(x-pixmap.width() / 2.0, y-height / 2.0, pixmap.width(), height)
                    label.show()
                    label.mouse_move_signal.connect(self.update_component_positions)
                    self.component_array.append(label)

        except (AttributeError, TypeError):
            pass

    def update_component_positions(self, component_name, component_position):
        pos = [str(component_position.x()), str(component_position.y())]
        self.current_model.set_control_position(component_name, pos)
