from PyQt5.QtWidgets import QToolBar


class PanelSwitchToolBar(QToolBar):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.setMovable(False)
        self.setFixedHeight(50)
        self.show()

