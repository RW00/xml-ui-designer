from PyQt5.QtCore import QObject, pyqtSlot, QAbstractItemModel, Qt, pyqtSignal
from PyQt5.QtWidgets import QMessageBox
from layoutparse import ScriptedUIPanelHolder, ScriptedUIBackgroundPanel


class FileMenuEventHandler(QObject):
    def __init__(self, panel_handle):
        super().__init__()
        self.panel_handle = panel_handle

    def register(self, signal):
        signal.connect(self.handle_actions)

    def update_tree(self, tree):
        self.panel = tree

    def handle_actions(self, *args):
        print(*args)
        try:
            sender = self.sender().text()
        except AttributeError as e:
            print(e)
            if self.sender().fileMode():
                self._handle_open(*args)
            else:
                self._handle_new(*args)
            return

        if sender == '&Open':
            self._handle_open(*args)
        elif sender == '&New':
            self._handle_new(*args)
        elif sender == '&Save':
            self._handle_save()

    def _handle_save(self):
        try:
            self.panel.write_to_xml(self.panel_handle.layout_xml)

            print('Saved file: {}'.format(self.panel_handle.layout_xml))

        except AttributeError:
            print('Nothing open to save!')
            return
        except Exception:
            print('Save failed! Must run with admin privileges!')

    def _handle_new(self, arg):
        layout_xml = arg

        if layout_xml:
            print('New layout file: {}'.format(layout_xml))
            self.panel_handle.open_layout_xml(layout_xml)

    def _handle_open(self, arg):
        layout_xml = arg
        print('Opening layout file: {}'.format(layout_xml))
        self.panel_handle.open_layout_xml(layout_xml)


class SwitchablePanelEventModel(QObject):
    tree_updated = pyqtSignal(ScriptedUIBackgroundPanel)

    def __init__(self, xml_panel_handle, panel_num):
        super().__init__()
        self.xml = xml_panel_handle
        self.panel_num = panel_num

    def setup(self):
        self._unpack_component_data()
    
    def get_background(self):
        self._unpack_component_data()
        return self.this_panel.background_image, self.this_panel.xy_offsets
         
    def get_control_data(self):
        return [control for control in self.controls]

    def get_control_positions(self):
        self.controls = self._unpack_component_data()
        return [control.xy_position for control in self.controls]
    
    def set_control_position(self, control_name, pos):
        for control in self.controls:
            if control.name == control_name:
                control.xy_position = pos
        self.tree_updated.emit(self.this_panel)

    def _unpack_component_data(self):
        self.panels = self.xml.get_subpanels()
        self.this_panel = self.panels[self.panel_num]
        #self.this_panel.image_path()
        self.controls = self.this_panel.get_controls()
