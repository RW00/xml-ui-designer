import sys

from PyQt5.QtWidgets import QApplication

import layoutparse as lp
from appmodel import FileMenuEventHandler, SwitchablePanelEventModel
from appwindow import MainWindow


class MainWindowController(object):
    def __init__(self):
        self.xml_panel_holder = lp.ScriptedUIPanelHolder()
        self.main_window = MainWindow()
        self.file_menu_handle = FileMenuEventHandler(self.xml_panel_holder)
        self._add_data_models()
        self._register_file_menu_handlers()

    def _register_file_menu_handlers(self):
        self.file_menu_handle.register(self.main_window.new_act_triggered)
        self.file_menu_handle.register(self.main_window.open_act_triggered)
        self.file_menu_handle.register(self.main_window.save_act_triggered)

    def _add_data_models(self):
        for i in range(self.xml_panel_holder.number_of_panels):
            model = SwitchablePanelEventModel(self.xml_panel_holder, i)
            model.tree_updated.connect(self.file_menu_handle.update_tree)
            self.main_window.add_main_widget_model(model)


def main():
    app = QApplication(sys.argv)
    main_window = MainWindowController()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
