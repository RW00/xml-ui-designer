from PyQt5.QtWidgets import QWidget, QLabel
from PyQt5.QtCore import Qt, pyqtSignal, QPoint
from PyQt5.QtGui import QPixmap


class ImageLabelWithMouseListener(QLabel):
    mouse_move_signal = pyqtSignal(str, QPoint)

    def __init__(self, parent, pixmap, name):
        QLabel.__init__(self, parent)
        
        self.setPixmap(pixmap)
        self.setObjectName(name)
        self.new_pos = None

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.initial_mouse_pos = event.globalPos()

    def mouseMoveEvent(self, event):
        super().mouseMoveEvent(event)

        if event.buttons() == Qt.LeftButton:
            mouse_move_pos = event.globalPos()
            diff = mouse_move_pos - self.initial_mouse_pos
            current_mouse_pos = self.mapToGlobal(self.pos())

            self.new_pos = self.mapFromGlobal(current_mouse_pos + diff)       
            self.move(self.new_pos)
            self.initial_mouse_pos = mouse_move_pos
        else:
            return

    def mouseReleaseEvent(self, event):
        if event.buttons() != Qt.LeftButton:
            if self.new_pos:
                self.mouse_move_signal.emit(self.objectName(), self.new_pos)
        else:
            return
