import os

from PyQt5.QtCore import QPoint, Qt, pyqtSignal
from PyQt5.QtGui import QDragMoveEvent, QIcon, QKeySequence, QPixmap
from PyQt5.QtWidgets import (QAction, QFileDialog, QLabel, QMainWindow,
                             QToolBar, QWidget)

from panelwindow import SwitchablePanelWindow


class MainWindow(QMainWindow):
    cwd = os.path.dirname(os.path.abspath(__file__))

    def __init__(self):
        super(MainWindow, self).__init__()
        self.window_bounds = (100, 100, 800, 600)
        self.panel_names = ['Panel1', 'Panel2', 'Panel3', 'Panel4', 'Panel5', 'Panel6']

        self.createMenus()
        self.createToolBars()
        self.createMainWidget()
        self.create_file_dialog()
        self.initUI()

    def initUI(self):
        self.setGeometry(*self.window_bounds)
        self.setWindowTitle('MPC Scripted UI Editor')
        self.show()

    def createMenus(self):
        menu_actions = self._create_menu_actions()
        file_menu = self.menuBar().addMenu('File')

        for act in menu_actions:
            file_menu.addAction(act)

    def createToolBars(self):
        file_tool_bar = self.addToolBar('panels')
        file_tool_bar.setFixedHeight(50)

        for act in self._create_toolbar_actions():
            file_tool_bar.addAction(act)

    def createMainWidget(self):
        self.panel_window = SwitchablePanelWindow()
        self.panel_window.setGeometry(0, 0, 600, 400)
        self.panel_window.set_current_panel(0)
        self.setCentralWidget(self.panel_window)

    def create_file_dialog(self):
        self.dialog = QFileDialog()
        self.open_act_triggered = self.dialog.fileSelected
        self.new_act_triggered = self.dialog.fileSelected
        
    def add_main_widget_model(self, model):
        self.panel_window.add_model(model)

    def on_new_act_triggered(self):
        self.dialog.setFileMode(QFileDialog.AnyFile)

        target_file, _ = self.dialog.getSaveFileName(self, "New layout xml", "C:\\Program Files\\Akai Pro\\MPC\\PluginLayouts")

        if target_file:
            self.new_act_triggered.emit(target_file)

    def on_open_act_triggered(self):
        self.dialog.setFileMode(QFileDialog.ExistingFile)

        target_file, _ = self.dialog.getOpenFileName(self, "Open layout xml file.", "C:\\Program Files\\Akai Pro\\MPC\\PluginLayouts")

        if target_file:
            self.open_act_triggered.emit(target_file)

        self._configure_panel_window(0)
        
    def on_save_action_triggered(self):
        pass

    def on_toolbar_action_triggered(self):
        sender = self.sender().text()
        panels = ['Panel1', 'Panel2', 'Panel3', 'Panel4', 'Panel5', 'Panel6']

        current_panel_index = panels.index(sender)
        self._configure_panel_window(current_panel_index)

    def _create_menu_actions(self):
        actions = list()

        new_action = QAction("&New", self)
        new_action.pyqtConfigure(shortcut=QKeySequence.New,
                                 statusTip="Create a new document",
                                 triggered=self.on_new_act_triggered)

        actions.append(new_action)

        open_action = QAction("&Open", self)
        open_action.pyqtConfigure(shortcut=QKeySequence.Open,
                                  statusTip="Open an existing document",
                                  triggered=self.on_open_act_triggered)

        actions.append(open_action)

        save_action = QAction("&Save", self)
        save_action.pyqtConfigure(shortcut=QKeySequence.Save,
                                  statusTip="Save the document to disk")

        actions.append(save_action)
 
        self.save_act_triggered = save_action.triggered
        #self.new_act_triggered = new_action.triggered

        return actions

    def _create_toolbar_actions(self):
        actions = list()

        for panel_name in self.panel_names:
            action = QAction(panel_name, self)
            action.pyqtConfigure(triggered=self.on_toolbar_action_triggered)
            actions.append(action)

        return actions

    def _configure_panel_window(self, current_panel_index):
        self.panel_window.set_current_panel(current_panel_index)
        self.panel_window.hide_images()
        self.panel_window.add_images()
