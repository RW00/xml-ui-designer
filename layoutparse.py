import xml.etree.ElementTree as ET
import os


###############################################################################
class ScriptedUILayoutRoot(object):
    def __init__(self, layout_xml_file):
        self.xml_layout_file = layout_xml_file
        self.tree = ET.parse(self.xml_layout_file)
        self.root = self.tree.getroot()
        self.base_image_path = os.path.dirname(self.xml_layout_file)

    def write_to_xml(self, layout_file):
        self.tree.write(self.xml_layout_file)


class ScriptedUIBackgroundPanel(ScriptedUILayoutRoot):
    def __init__(self, layout_xml_file, panel_num):
        super(ScriptedUIBackgroundPanel, self).__init__(layout_xml_file)

        self.set_panel_id(panel_num)
        self._find_panel_xml_node()
        self._create_child_components_from_tree()
        self.x_offset = ''
        self.y_offset = ''
        self.background_image_path = ''
        self.background_selected_image_path = ''

    @property
    def panel_name(self):
        self.name = self.panel.attrib['name']

        return self.name

    @panel_name.setter
    def panel_name(self, name):
        if type(name) is not str:
            raise TypeError("name must be a string.")

        self.name = name

    @property
    def background_image(self):
        self.background_image_path = self.panel.find('Background')
        # this stupid file path formatting is because QFileDialog
        # gives us weirdly formatted file paths
        # TODO: look into tokenising the incoming path to
        # shove in os.path.join(), for the sake of sanity.
        return self.base_image_path + '/' + self.background_image_path.text

    @background_image.setter
    def background_image(self, image_path, image_path_selected):
        self.background_image_path = image_path
        self.background_selected_image_path = image_path_selected

    @property
    def xy_offsets(self):
        return self.x_offset, self.y_offset

    @xy_offsets.setter
    def xy_offsets(self, x_offset, y_offset):
        self.x_offset = x_offset
        self.y_offset = y_offset

# Public Methods
    def set_panel_id(self, panel_id):
        if type(panel_id) is not int:
            raise TypeError("panel_id must be an int")

        if panel_id not in range(1, 7):
            raise ValueError("panel_id must be between 1 and 6")

        self.num = str(panel_id)

    def get_controls(self):
        return self.controls

# Protected Methods
    def _find_panel_xml_node(self):
        for panel in self.root.iter('Panel'):
            if panel.attrib['number'] == self.num:
                self.panel = panel

    def _create_child_components_from_tree(self):
        self.controls = []

        for child_component in self.panel:
            if child_component.tag == 'Knob':
                component = Knob(child_component.attrib['name'],
                                 child_component,
                                 self.base_image_path)
                self.controls.append(component)

            if child_component.tag == 'Button':
                component = Button(child_component.attrib['name'],
                                   child_component,
                                   self.base_image_path)
                self.controls.append(component)

            if child_component.tag == 'ComboBox':
                component = ComboBox(child_component.attrib['name'],
                                     child_component,
                                     self.base_image_path)
                self.controls.append(component)


###############################################################################
class ScriptedUIControlComponent(object):
    def __init__(self, name, node, base_img_path):
        self.node = node
        self.name = name
        self.base_image_path = base_img_path
        self._unpack_node()

    def __getattr__(self, name):
        return ''

    def _unpack_node(self):
        self.img = self.node.find('Image')
        self.x_pos = self.node.find('XPos')
        self.y_pos = self.node.find('YPos')
        self.param = self.node.find('Parameter')
        self.qlink = self.node.find('QLink')

    @property
    def image_path(self):
        img_text = self.img.text
        if img_text:
            return self.base_image_path + '/' + img_text
        else:
            return ''

    @image_path.setter
    def image_path(self, new_path):
        self.img = new_path

    @property
    def xy_position(self):
        return [self.x_pos.text, self.y_pos.text]

    @xy_position.setter
    def xy_position(self, pos):
        self.x_pos.text = pos[0]
        self.y_pos.text = pos[1]

    @property
    def parameter(self):
        return self.param.text

    @parameter.setter
    def parameter(self, new_val):
        self.param = new_val

    @property
    def q_link(self):
        return self.qlink.text

    @q_link.setter
    def q_link(self, new_val):
        if type(new_val) is not type(int):
            raise TypeError("Value must be an int")

        if new_val not in range(1, 16):
            raise ValueError("Value must be within the range 1-16")

        self.qlink = new_val


class Knob(ScriptedUIControlComponent):
    def __init__(self, name, node, bpath):
        super(Knob, self).__init__(name, node, bpath)
        self.num_frames = self.node.find('NumFrames')


class Button(ScriptedUIControlComponent):
    def __init__(self, name, node, bpath):
        super(Button, self).__init__(name, node, bpath)
        self.image_selected = self.node.find('ImageSelected')


class ComboBox(ScriptedUIControlComponent):
    def __init__(self, name, node, bpath):
        super(ComboBox, self).__init__(name, node, bpath)
        self.txt_colour = self.node.find('TextColor')
        self.width = self.node.find('Width')
        self.height = self.node.find('Height')
        self.items = self.node.findall('Item')


###############################################################################
class ScriptedUIPanelHolder(object):
    def __init__(self):
        self._num_panels = 6
        self.layout_xml = ''

    @property
    def number_of_panels(self):
        return self._num_panels

    def open_layout_xml(self, layout_xml):
        self.layout_xml = layout_xml

    def get_subpanels(self):
        if not self.layout_xml:
            raise AttributeError("Must open a layout xml file before calling!")

        self.panels = [ScriptedUIBackgroundPanel(self.layout_xml,
                                                 panel_num=i+1)
                       for i in range(self._num_panels)]

        return self.panels
